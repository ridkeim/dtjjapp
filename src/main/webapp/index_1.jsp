﻿<%-- 
    Document   : index
    Created on : 16.12.2016, 17:40:45
    Author     : ridkeim
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css">
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/jquery.validate.min.js" type="text/javascript"></script>
        <script type="text/javascript">
        </script>
    </head>
    <body>
        <div id="content">
            <div id="form">
                <form method='post' action='findA'>
                    <c:choose>
                        <c:when test="${empty texts}">
                            Текст 1: <input type='text' name='texts'/><br/><br/>
                            Текст 2: <input type='text' name='texts'/><br/><br/>
                            Текст 3: <input type='text' name='texts'/><br/><br/>
                            Текст 4: <input type='text' name='texts'/><br/><br/>
                            Текст 5: <input type='text' name='texts'/><br/><br/>
                        </c:when>
                        <c:otherwise>
                        <c:forEach var="text" items="${texts}" varStatus="loop">
                            Текст ${loop.index+1}: <input type='text' name='texts' value="${text.text}"/>
                            <br/>
                            <c:if test="${not text.valid}">
                            <span>${text.msg}</span>
                            <br/>
                            </c:if>
                            <br/>
                        </c:forEach>
                        </c:otherwise>
                    </c:choose>
                    <div id="button">
                        <input type='submit' value='Поиск пересечений'/><br/>
                    </div>
                </form>
            </div>
            <c:if test="${not empty msg}">
                ${msg}
            </c:if>
            <c:if test="${not empty result}">
            <div id="result">                    
                <table>
                    <tr>
                        <th>Слово</th>
                        <th>Количество текcтов</th>
                        <th>Номера текстов</th>                            
                    </tr>
                    <c:forEach var="res" items="${result}" varStatus="loop">
                    <tr class='${(loop.index % 2 == 0) ? 'grey_row' : 'white_row'}'>
                        <td>${res.wordData}</td>
                        <td>${res.textsCount}</td>
                        <td>${res.textsSubIds}</td>
                    </tr>                            
                    </c:forEach>                        
                </table>                
            </div>
            </c:if>
        </div>
    </body>
</html>
