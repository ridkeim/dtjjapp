/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ridkeim.dtjjapp.controller;

import static com.ridkeim.dtjjapp.controller.MainServlet.ATTRIBUTE_NAME_MSG;
import static com.ridkeim.dtjjapp.controller.MainServlet.ATTRIBUTE_NAME_TEXT;
import com.ridkeim.dtjjapp.validator.Validator;
import com.ridkeim.dtjjapp.data.Text;
import com.ridkeim.dtjjapp.entity.Result;
import com.ridkeim.dtjjapp.session.TaskManager;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedThreadFactory;
import javax.ejb.EJB;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author ridkeim
 */
@WebServlet(name = "MainAsyncServlet", asyncSupported = true, urlPatterns = {"/findA", "/checkA", "/resultA"})
public class MainAsyncServlet extends HttpServlet {

    @EJB
    private TaskManager taskManager;
    
    @Resource
    private ManagedExecutorService executorService;
    static String url_checkStatus = "/checkA";
    static String url_find = "/findA";
    static String url_result = "/resultA";
    static String url_forward = "/index_1.jsp";

    final static String PARAMETER_NAME_TEXT = "texts";
    final static String ATTRIBUTE_NAME_TEXT = "texts";
    final static String ATTRIBUTE_NAME_RESULT = "result";
    final static String ATTRIBUTE_NAME_ERROR = "error";
    final static String ATTRIBUTE_NAME_TASK_ID = "task_id";
    final static String ATTRIBUTE_NAME_MSG = "msg";

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        loadResult(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        performSearch(request,response);
//        try {
//            request.getRequestDispatcher(url_forward).forward(request, response);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
    }

    

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void loadResult(final HttpServletRequest request, HttpServletResponse response) {
        final AsyncContext ctx = request.startAsync();
        ctx.setTimeout(20*60*1000);
        runOnExecutor(ctx, new Runnable() {
            public void run() {
                List<Result> res = taskManager.findResult(1);
                request.setAttribute(ATTRIBUTE_NAME_RESULT, res);
                ctx.dispatch(url_forward);
            }
        });
    }
    
    private void runOnExecutor(final AsyncContext ctx, final Runnable work) {
        executorService.submit(new Runnable(){
            @Override
            public void run() {
                try {
                    work.run();
                } catch (RuntimeException ex) {
                    HttpServletRequest request = (HttpServletRequest) ctx.getRequest();
                    request.setAttribute(ATTRIBUTE_NAME_MSG, taskManager.MSG_TRY_AGAIN);
                    ctx.dispatch(url_forward);
                    Logger.getLogger(MainAsyncServlet.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    private void performSearch(final HttpServletRequest request, HttpServletResponse response) {
        final AsyncContext ctx = request.startAsync();
        ctx.setTimeout(20*60*1000);
        runOnExecutor(ctx, new Runnable() {
            public void run() {
//                try {
//                    Thread.sleep(5000);
//                } catch (InterruptedException ex) {
//                    Logger.getLogger(MainAsyncServlet.class.getName()).log(Level.SEVERE, null, ex);
//                }
                Text[] texts = (Text[]) request.getAttribute(ATTRIBUTE_NAME_TEXT);
                String[] param = request.getParameterValues(PARAMETER_NAME_TEXT);
                if (texts == null) {
                    texts = new Text[param.length];
                    for (int i = 0; i < texts.length; i++) {
                        texts[i] = new Text();
                    }
                }
                for (int i = 0; i < texts.length; i++) {
                    texts[i].setText(param[i]);
                }
                Validator val = new Validator();
                boolean erfl = val.validate(texts);
                request.setAttribute(ATTRIBUTE_NAME_TEXT, texts);
                if (erfl) {
                    request.setAttribute(ATTRIBUTE_NAME_ERROR, erfl);
                } else {
                    int id = taskManager.placeAndPerformTask(texts);
                    if (id > 0) {
                        List<Result> result = taskManager.findResult(id);
                        if(result.isEmpty()){
                            request.setAttribute(ATTRIBUTE_NAME_MSG, taskManager.MSG_NOT_FOUND);
                        }else{
                            request.setAttribute(ATTRIBUTE_NAME_RESULT, result);   
                        }
                    }else {
                        request.setAttribute(ATTRIBUTE_NAME_MSG, taskManager.MSG_ERROR);
                    }
                }
                ctx.dispatch(url_forward);
            }
        });
    }
}
