/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ridkeim.dtjjapp.controller;

import com.ridkeim.dtjjapp.validator.Validator;
import com.ridkeim.dtjjapp.data.Text;
import com.ridkeim.dtjjapp.entity.Result;
import com.ridkeim.dtjjapp.session.TaskManager;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedThreadFactory;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author ridkeim
 */
@WebServlet(name = "MainServlet", urlPatterns = {"/find", "/check", "/result"})
public class MainServlet extends HttpServlet {

    @EJB
    private TaskManager taskManager;
    static String url_checkStatus = "/check";
    static String url_find = "/find";
    static String url_result = "/result";
    static String url_forward = "/index.jsp";

    final static String PARAMETER_NAME_TEXT = "texts";
    final static String ATTRIBUTE_NAME_TEXT = "texts";
    final static String ATTRIBUTE_NAME_RESULT = "result";
    final static String ATTRIBUTE_NAME_ERROR = "error";
    final static String ATTRIBUTE_NAME_TASK_ID = "task_id";
    final static String ATTRIBUTE_NAME_MSG = "msg";

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String userPath = request.getServletPath();
        if (userPath.equals(url_checkStatus)) {
            // TODO: check Status
        } else if (userPath.equals(url_result)) {
            
        }
        try {
            request.getRequestDispatcher(url_forward).forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String userPath = request.getServletPath();
        if (userPath.equals(url_find)) {
            Text[] texts = (Text[]) request.getAttribute(ATTRIBUTE_NAME_TEXT);
            String[] param = request.getParameterValues(PARAMETER_NAME_TEXT);
            if (texts == null) {
                texts = new Text[param.length];
                for (int i = 0; i < texts.length; i++) {
                    texts[i] = new Text();
                }
            }
            for (int i = 0; i < texts.length; i++) {
                texts[i].setText(param[i]);
            }
            Validator val = new Validator();
            boolean erfl = val.validate(texts);
            request.setAttribute(ATTRIBUTE_NAME_TEXT, texts);
            if (erfl) {
                request.setAttribute(ATTRIBUTE_NAME_ERROR, erfl);
            } else {
                performSerch(request, texts);
            }
        }
        try {
            request.getRequestDispatcher(url_forward).forward(request, response);
        } catch (ServletException | IOException ex) {
        }
    }

    void performSerch(HttpServletRequest request, Text[] t) {
        int id = taskManager.placeAndPerformTask(t);
        if (id > 0) {
            List<Result> result = taskManager.findResult(id);
            if(result.isEmpty()){
                request.setAttribute(ATTRIBUTE_NAME_MSG, taskManager.MSG_NOT_FOUND);
            }else{
                request.setAttribute(ATTRIBUTE_NAME_RESULT, result);   
            }
        }else {
            request.setAttribute(ATTRIBUTE_NAME_MSG, taskManager.MSG_ERROR);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
