/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ridkeim.dtjjapp.controller;

import com.ridkeim.dtjjapp.entity.Tasks;
import com.ridkeim.dtjjapp.session.TaskManager;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author ridkeim
 */
public class MyWorker implements Runnable{
    
    private int id;
    
    public MyWorker(int id) {
        this.id = id;
    }
    
    @Override
    public void run() {
        try {
            Context ctx = new InitialContext();
            TaskManager taskmanager = (TaskManager) ctx.lookup("java:module/TaskManager");
            int performTask = taskmanager.performTask(id);
            System.out.println("Tried perform task "+performTask);
        } catch (NamingException ex) {
            Logger.getLogger(MyWorker.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        } catch (Exception ex) {
            Logger.getLogger(MyWorker.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
    }
    
}
