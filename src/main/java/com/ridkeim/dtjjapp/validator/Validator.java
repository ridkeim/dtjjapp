/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ridkeim.dtjjapp.validator;

import com.ridkeim.dtjjapp.data.Text;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author ridkeim
 */
public class Validator {

    public boolean validate(String[] param) {
        for (String param1 : param) {
            String trim = param1.trim();
            if(trim.isEmpty() || trim.codePointCount(0, trim.length())>1000){
                return true;
            };
        }
        return false;
    }

    public boolean validate(Text[] texts) {
        if(texts!=null && texts.length>0){
            for (Text text : texts) {
                if(!text.isValid()){
                    return true;
                }
            }
        }
        return false;
    }
    static Pattern p = Pattern.compile("[a-zа-я0-9]{1}",Pattern.CASE_INSENSITIVE);
    public boolean validateWord(String word){
        int count=0;
        int min_count = 3;
        Matcher m = p.matcher(word);
        while (m.find() && count<min_count) {
            count++;
        }
        return (count==min_count);
    };
    
}
