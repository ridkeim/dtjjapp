/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ridkeim.dtjjapp.session;

import com.ridkeim.dtjjapp.data.Text;
import com.ridkeim.dtjjapp.entity.Result;
import com.ridkeim.dtjjapp.entity.Tasks;
import com.ridkeim.dtjjapp.entity.Texts;
import com.ridkeim.dtjjapp.entity.Words;
import com.ridkeim.dtjjapp.validator.Validator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author ridkeim
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class TaskManager {
    @PersistenceContext(unitName = "dtjjPU")
    private EntityManager em;
    @Resource
    private SessionContext context;
    @EJB
    private TasksFacade tasksFacade;
    @EJB
    private WordsFacade wordsFacade;
    @EJB
    private TextsFacade textsFacade;
    @EJB
    private ResultFacade resultFacade;
    
    public final static String MSG_OK = "All`s ok";
    public final static String MSG_ERROR = "Get error while processing serch";
    public final static String MSG_TRY_AGAIN = "Something went wrong, try again later.";
    public final static String MSG_NOT_FOUND = "No result found.";
    
    
    private Tasks addTask(){
        Tasks t = new Tasks();
        String state = "placed";
        t.setState(state);
        tasksFacade.create(t);
        return t;
    };
    
    private Texts addTexts(Text text, Tasks task, int sub_id){
        Texts texts = new Texts();
        texts.setSubId(sub_id);
        texts.setTasksId(task);
        texts.setTextData(text.getText());
        textsFacade.create(texts);
        return texts;
    };
    
    private HashMap<String, HashSet<Texts>> findWordsUsage(Collection<Texts> textsCollection) {
         // same as " []{}();:,.‒…!?-\"«»„““”‹›"
        String reg = "[\\u0020\\u005b\\u005d\\u007b\\u007d\\u0028\\u0029\\u003b"
                + "\\u003a\\u002c\\u002e\\u2012\\u2026\\u0021\\u003f\\u002d"
                + "\\u0022\\u00ab\\u00bb\\u201e\\u201c\\u201d\\u2039\\u203a]{1,}";
        HashMap<String, HashSet<Texts>> map = new HashMap<>();
        Validator v = new Validator();
        for (Texts text : textsCollection) {
            for (String word : text.getTextData().toLowerCase().split(reg)) {
                if(v.validateWord(word)){
                    HashSet<Texts> set;
                    if(map.containsKey(word)){
                        set=map.get(word);
                    }else{
                        set=new HashSet<>();
                    }
                    boolean add = set.add(text);
                    map.put(word,set);
                }
            }
        }
        return map;
    }    
    private Words addWord(String key){
        Words word;
        List<Words> findByData = wordsFacade.findByData(key);
        if(findByData.isEmpty()){
            word = new Words();
            word.setWordData(key);
            wordsFacade.create(word);
        }else{
            word = findByData.get(0);
        }
        return word;
    }

    public List<Result> findResult(int id){
        return resultFacade.getResultStored(id, 20);
    }
    
    public void saveResult(HashMap<String,HashSet<Texts>> map){
        for (Map.Entry<String, HashSet<Texts>> entrySet : map.entrySet()) {
            String key = entrySet.getKey();
            HashSet<Texts> value = entrySet.getValue();
            if(value.size()<2) continue;
            Words word = addWord(key);
            Collection<Texts> texts = word.getTextsCollection();
            if(texts==null){
                word.setTextsCollection(value);
            }else{
                texts.addAll(value);
            }
            Iterator<Texts> iterator = value.iterator();
            while (iterator.hasNext()) {
                Texts next = iterator.next();
                next.getWordsCollection().add(word);
            }
        }
    };
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public int placeTask(Text[] texts){
        try {
            Tasks t = addTask();
            for (int i = 0; i < texts.length; i++) {
                addTexts(texts[i], t, i+1);
            }
            em.flush();
            return t.getId();
        } catch (Exception e) {
            context.setRollbackOnly();
        }
        return 0;
    };
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public int performTask(int id){
        try{
            Tasks task = tasksFacade.getTaskById(id);        
            Collection<Texts> textsCollection = task.getTextsCollection();
            HashMap<String,HashSet<Texts>> map = findWordsUsage(textsCollection);
            saveResult(map);
            task.setState("done");
            em.flush();
            return task.getId();
        }catch(Exception e){
            context.setRollbackOnly();
        }
        return 0;
    };
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public int placeAndPerformTask(Text[] t){
        try{            
            Tasks task = addTask();
            ArrayList<Texts> text = new ArrayList<>();
            for (int i = 0; i < t.length; i++) {
                text.add(addTexts(t[i], task, i+1));
            }
            em.flush();
            HashMap<String,HashSet<Texts>> map= findWordsUsage(text);
            saveResult(map);
            task.setState("done");
            em.flush();
            return task.getId();
        }catch(Exception e){
            context.setRollbackOnly();
        }
        return 0;
    };
   
}
