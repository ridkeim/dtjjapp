/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ridkeim.dtjjapp.session;

import com.ridkeim.dtjjapp.entity.Result;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;


/**
 *
 * @author ridkeim
 */
@Stateless
public class ResultFacade extends AbstractFacade<Result>{
    @PersistenceContext(unitName = "dtjjPU")
    private EntityManager em;

    public ResultFacade() {
        super(Result.class);
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    
    public List<Result> getResultStored(int task_id, int limit){
        StoredProcedureQuery createdQuery = getEntityManager().createNamedStoredProcedureQuery("getResult");
        createdQuery.setParameter("taskId", task_id);
        createdQuery.setParameter("limitCount", limit);
        List<Result> result = createdQuery.getResultList();
        return result;
    }
    
    
    public List<Result> getResult(int task_id, int limit){
        StoredProcedureQuery createdQuery = getEntityManager().createStoredProcedureQuery("getResultByTaskId", Result.class);
        createdQuery.registerStoredProcedureParameter("taskId", Integer.class, ParameterMode.IN);
        createdQuery.registerStoredProcedureParameter("limitCount", Integer.class, ParameterMode.IN);
        createdQuery.setParameter("taskId", task_id);
        createdQuery.setParameter("limitCount", limit);
        boolean execute = createdQuery.execute();
        List<Result> result = createdQuery.getResultList();
        return result;
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
}
