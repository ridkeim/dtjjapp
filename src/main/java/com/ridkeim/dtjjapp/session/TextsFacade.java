/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ridkeim.dtjjapp.session;

import com.ridkeim.dtjjapp.entity.Texts;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

/**
 *
 * @author ridkeim
 */
@Stateless
public class TextsFacade extends AbstractFacade<Texts> {
    @PersistenceContext(unitName = "dtjjPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TextsFacade() {
        super(Texts.class);
    }
    
    public List<Texts> getTexts(int task_id){
        StoredProcedureQuery createdQuery = getEntityManager().createStoredProcedureQuery("getTextsByTaskId", Texts.class);
        createdQuery.registerStoredProcedureParameter("taskId", Integer.class, ParameterMode.IN);
        createdQuery.setParameter("taskId", task_id);
        boolean execute = createdQuery.execute();
        List<Texts> result = createdQuery.getResultList();
        return result;
    }
    
    
}
