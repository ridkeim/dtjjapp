/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ridkeim.dtjjapp.session;

import com.ridkeim.dtjjapp.entity.Words;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 *
 * @author ridkeim
 */
@Stateless
public class WordsFacade extends AbstractFacade<Words> {
    @PersistenceContext(unitName = "dtjjPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public WordsFacade() {
        super(Words.class);
    }
    
    public List<Words> findByData(String data){
        Query createNamedQuery = em.createNamedQuery("Words.findByWordData");
        createNamedQuery.setParameter("wordData", data);
        List<Words> result = createNamedQuery.getResultList();
        return result;
    };
     public Words findById(int id){
        Query createNamedQuery = em.createNamedQuery("Words.findById");
        createNamedQuery.setParameter("id", id);
        Words result = null;
        try{
            result = (Words) createNamedQuery.getSingleResult();
        }catch(NoResultException e){
            /*NOP*/
        }
        return result;
    };
}
