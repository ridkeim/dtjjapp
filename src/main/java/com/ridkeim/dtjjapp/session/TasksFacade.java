/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ridkeim.dtjjapp.session;

import com.ridkeim.dtjjapp.entity.Tasks;
import com.ridkeim.dtjjapp.entity.Words;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author ridkeim
 */
@Stateless
public class TasksFacade extends AbstractFacade<Tasks> {
    @PersistenceContext(unitName = "dtjjPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TasksFacade() {
        super(Tasks.class);
    }
    
    public Tasks getTaskById(int id){
        Query createNamedQuery = em.createNamedQuery("Tasks.findById");
        createNamedQuery.setParameter("id", id);
        Tasks result = null;
        try{
            result = (Tasks) createNamedQuery.getSingleResult();
        }catch(NoResultException e){
            /*NOP*/
        }
        return result;
    };
    
    public List<Tasks> getTaskByState(String state){
        Query createNamedQuery = em.createNamedQuery("Tasks.findByState");
        createNamedQuery.setParameter("state", state);
        List<Tasks> resultList = createNamedQuery.getResultList();
        return resultList;
    };
    
}
