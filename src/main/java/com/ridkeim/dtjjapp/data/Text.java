/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ridkeim.dtjjapp.data;

import java.io.Serializable;

/**
 *
 * @author ridkeim
 */
public class Text implements Serializable{
    static String[] msg = new String[]{
        "",
        "Количество символов должныо быть меньше 1000.",
        "Текст не может быть пустым.",
        
    };
    static int ERROR_NONE = 0;
    static int ERROR_TOO_MANY_CHARACHTERS = 1;
    static int ERROR_EMPTY_FIELD = 2;    
    String text;
    private int error_code;
    boolean valid;

    public Text() {
        text = "";
        error_code = ERROR_EMPTY_FIELD;
        valid = false;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text.trim();
//        System.out.println("length="+this.text.length());
//        System.out.println("codeCount"+this.text.codePointCount(0, this.text.length()));
        validate();
    }

    public boolean isValid() {
        return valid;
    }

    public String getMsg() {
        return msg[error_code];
    }
    
    private void validate() {
        int size = text.codePointCount(0, text.length());
        error_code = ERROR_NONE;
        valid = true;
        if (size > 1000) {
            error_code = ERROR_TOO_MANY_CHARACHTERS;
            valid = false;
        } else if (size == 0) {
            error_code = ERROR_EMPTY_FIELD;
            valid = false;
        }
    }
    public static void main(String[] args) {
        String t = "ö ssssss👽💔";
        String t1 = "acc";
        String t2 = "aaa";
        System.out.println(t1.compareTo(t2));
    }
    
    

}
