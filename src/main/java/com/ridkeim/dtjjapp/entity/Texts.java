/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ridkeim.dtjjapp.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ridkeim
 */
@Entity
@Table(name = "texts")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Texts.findAll", query = "SELECT t FROM Texts t"),
    @NamedQuery(name = "Texts.findById", query = "SELECT t FROM Texts t WHERE t.id = :id"),
    @NamedQuery(name = "Texts.findByTextData", query = "SELECT t FROM Texts t WHERE t.textData = :textData"),
    @NamedQuery(name = "Texts.findBySubId", query = "SELECT t FROM Texts t WHERE t.subId = :subId")})
public class Texts implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1000)
    @Column(name = "text_data")
    private String textData;
    @Basic(optional = false)
    @NotNull
    @Column(name = "sub_id")
    private int subId;
    @JoinTable(name = "texts_has_words", joinColumns = {
        @JoinColumn(name = "texts_id", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "words_id", referencedColumnName = "id")})
    @ManyToMany
    private Collection<Words> wordsCollection;
    @JoinColumn(name = "task_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Tasks tasksId;

    public Texts() {
    }

    public Texts(Integer id) {
        this.id = id;
    }

    public Texts(Integer id, String textData, int subId) {
        this.id = id;
        this.textData = textData;
        this.subId = subId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTextData() {
        return textData;
    }

    public void setTextData(String textData) {
        this.textData = textData;
    }

    public int getSubId() {
        return subId;
    }

    public void setSubId(int subId) {
        this.subId = subId;
    }

    @XmlTransient
    public Collection<Words> getWordsCollection() {
        return wordsCollection;
    }

    public void setWordsCollection(Collection<Words> wordsCollection) {
        this.wordsCollection = wordsCollection;
    }

    public Tasks getTasksId() {
        return tasksId;
    }

    public void setTasksId(Tasks tasksId) {
        this.tasksId = tasksId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Texts)) {
            return false;
        }
        Texts other = (Texts) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ridkeim.dtjjapp.entity.Texts[ id=" + id + " ]";
    }
    
}
