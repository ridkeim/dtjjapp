/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ridkeim.dtjjapp.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author ridkeim
 */
@Entity
@NamedStoredProcedureQueries({
    @NamedStoredProcedureQuery(
	name = "getResult",
	procedureName = "getResultByTaskId", 
	resultClasses = Result.class, 
	parameters = {
		@StoredProcedureParameter(mode = ParameterMode.IN, type = Integer.class,name="taskId"), 
		@StoredProcedureParameter(mode = ParameterMode.IN, type = Integer.class,name="limitCount")
    })
})        
public class Result implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1000)
    @Column(name = "word_data")
    private String wordData;
    @Basic(optional = false)
    @NotNull
    @Column(name = "texts_count")
    private long textsCount;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "texts_sub_ids")
    private String textsSubIds;

    public Result() {
    }

    public Result(Integer id) {
        this.id = id;
    }

    public Result(Integer id, String wordData, long textsCount, String textsSubIds) {
        this.id = id;
        this.wordData = wordData;
        this.textsCount = textsCount;
        this.textsSubIds = textsSubIds;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getWordData() {
        return wordData;
    }

    public void setWordData(String wordData) {
        this.wordData = wordData;
    }

    public long getTextsCount() {
        return textsCount;
    }

    public void setTextsCount(long textsCount) {
        this.textsCount = textsCount;
    }

    public String getTextsSubIds() {
        return textsSubIds;
    }

    public void setTextsSubIds(String textsSubIds) {
        this.textsSubIds = textsSubIds;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Result)) {
            return false;
        }
        Result other = (Result) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ridkeim.dtjjapp.entity.Result[ id=" + id + " ]";
    }
    
}
