/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ridkeim.dtjjapp.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ridkeim
 */
@Entity
@Table(name = "words")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Words.findAll", query = "SELECT w FROM Words w"),
    @NamedQuery(name = "Words.findById", query = "SELECT w FROM Words w WHERE w.id = :id"),
    @NamedQuery(name = "Words.findByWordData", query = "SELECT w FROM Words w WHERE w.wordData = :wordData")})
public class Words implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1000)
    @Column(name = "word_data")
    private String wordData;
    @ManyToMany(mappedBy = "wordsCollection")
    private Collection<Texts> textsCollection;

    public Words() {
    }

    public Words(Integer id) {
        this.id = id;
    }

    public Words(Integer id, String wordData) {
        this.id = id;
        this.wordData = wordData;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getWordData() {
        return wordData;
    }

    public void setWordData(String wordData) {
        this.wordData = wordData;
    }

    @XmlTransient
    public Collection<Texts> getTextsCollection() {
        return textsCollection;
    }

    public void setTextsCollection(Collection<Texts> textsCollection) {
        this.textsCollection = textsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Words)) {
            return false;
        }
        Words other = (Words) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ridkeim.dtjjapp.entity.Words[ id=" + id + " ]";
    }
    
}
