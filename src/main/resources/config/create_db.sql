-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema dtjj
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `dtjj` ;

-- -----------------------------------------------------
-- Schema dtjj
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `dtjj` DEFAULT CHARACTER SET utf8mb4 ;
USE `dtjj` ;

-- -----------------------------------------------------
-- Table `dtjj`.`tasks`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dtjj`.`tasks` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `state` VARCHAR(10) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `dtjj`.`texts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dtjj`.`texts` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `text_data` VARCHAR(1000) NOT NULL,
  `task_id` INT NOT NULL,
  `sub_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_text_task_idx` (`task_id` ASC),
  CONSTRAINT `fk_text_task`
    FOREIGN KEY (`task_id`)
    REFERENCES `dtjj`.`tasks` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `dtjj`.`words`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dtjj`.`words` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `word_data` VARCHAR(1000) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `dtjj`.`texts_has_words`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dtjj`.`texts_has_words` (
  `texts_id` INT NOT NULL,
  `words_id` INT NOT NULL,
  PRIMARY KEY (`texts_id`, `words_id`),
  INDEX `fk_texts_has_words_words1_idx` (`words_id` ASC),
  INDEX `fk_texts_has_words_texts1_idx` (`texts_id` ASC),
  CONSTRAINT `fk_texts_has_words_texts1`
    FOREIGN KEY (`texts_id`)
    REFERENCES `dtjj`.`texts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_texts_has_words_words1`
    FOREIGN KEY (`words_id`)
    REFERENCES `dtjj`.`words` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;

USE `dtjj` ;

-- -----------------------------------------------------
-- procedure getResultByTaskId
-- -----------------------------------------------------

DELIMITER $$
USE `dtjj`$$
CREATE PROCEDURE getResultByTaskId (in taskId int, in limit_count int) 
BEGIN
	select max(words.id) as id, word_data, count(*) as texts_count, group_concat(texts.sub_id order by texts.sub_id asc separator '; ') as texts_sub_ids
		from texts
			join texts_has_words
				on (texts.id = texts_has_words.texts_id)
			join words
				on (texts_has_words.words_id = words.id)
		where texts.task_id = taskId 
        GROUP BY words.id
        ORDER BY texts_count desc, words.word_data asc
        LIMIT limit_count;
END$$

DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
